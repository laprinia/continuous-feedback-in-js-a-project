const Sequelize=require('sequelize');

const sequelize =new Sequelize('continuous', 'user', 'passw', {
  dialect: 'mysql',

});

sequelize.authenticate().then( ()=>
{
console.log("Conn estabilished");
}).catch(err =>{
  console.log(`Err on conn: ${err}`);
})

class Teacher extends Sequelize.Model {}
Teacher.init({

  fullName : {
    type :Sequelize.STRING,
    allowNull: false
  },
  email :{
    type: Sequelize.STRING,
    allowNull :false

  },
    password: {
    type:Sequelize.STRING,
    allowNull:false,
    validate:{
    len : {
      args: [8,32],
      msg : "Password must be at least 8 characters"
          }
          }
            }

  },{sequelize, modelName: 'teachers',timestamps:false});

class Student extends Sequelize.Model {}
Student.init({


   name:{
     type:Sequelize.STRING,
     allowNull:false
   },
   email :{
    type: Sequelize.STRING,
    allowNull :false,
    validate : {
      isEmail: true
    }
  },
  password: {
  type:Sequelize.STRING,
  allowNull:false,
  validate:{
  len : {
    args: [8,32],
    msg : "Password must be at least 8 characters"
        }
        }
          },
  gradeYear:{
    type:Sequelize.STRING,
    allowNull: true

  }

}

,{sequelize, modelName: 'students',timestamps: false});

  class ActivitiesList extends Sequelize.Model {}
  ActivitiesList.init({

     activityID:{
       type: Sequelize.INTEGER,
       allowNull : false

   },
   teacherID : {
     allowNull: false,
     type: Sequelize.INTEGER

   }
 },

 {sequelize, modelName: 'activitieslist',timestamps:false});

 class Activity extends Sequelize.Model {}
 Activity.init({


    name:{
      type:Sequelize.STRING,
      allowNull:false
    },

    startingDate: {
      type: Sequelize.STRING,
      allowNull:false
    },
    endingDate: {
      type: Sequelize.STRING,
      allowNull:false
    },

    teacherID : {

      type: Sequelize.INTEGER,
      allowNull: true

    },
    description: {
      type: Sequelize.STRING,
      allowNull: false
    },
    key:{
      type:Sequelize.STRING,
      allowNull:false
    },
    smile:{
      type: Sequelize.INTEGER,
      allowNull: true
    },
    frown:{
      type: Sequelize.INTEGER,
      allowNull: true
    },
    suprise:{
      type: Sequelize.INTEGER,
      allowNull: true

    },
    confuse:{
      type: Sequelize.INTEGER,
      allowNull: true

    }

},{sequelize, modelName: 'activities'});

//legatura m la m
Teacher.belongsToMany(Activity, {through: 'ActivitiesList'});
Activity.belongsToMany(Teacher, {through: 'ActivitiesList'});
//legatura 1 la m
Activity.belongsTo(Student);
Student.hasMany(Activity);

Teacher.sync();
Student.sync();
Activity.sync();
ActivitiesList.sync();

module.exports= {
  sequelize,
  Teacher,
  Student,
  Activity,
  ActivitiesList
}
