'use strict'
const express=require('express');
const Sequelize= require('sequelize');
const bodyParser=require('body-parser')
var cors = require('cors')
const port=3236;

const {sequelize, Teacher, Activity, ActivitiesList,Student}= require('./models');


const app=express();


app.use(bodyParser.json());
app.use(cors())


app.post('/register-teachers', (req, res) => {
    const teacher = req.body;
    if(teacher.fullName &&teacher.email && teacher.password) {
        Teacher.findOne({where: {email: teacher.email}}).then(response => {
            if(response) {
               res.status(409).send({
                   message: "Teacher email already in use"
               })
            } else {
              Teacher.create(teacher).then(()=> {
                res.status(201).send({
                  message: 'Teacher inserted!'
                });
            })
        }
      })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        });
    }

})



app.post('/make-activity', (req, res) => {
    const a = req.body;
    if(a.name &&a.startingDate && a.endingDate && a.description && a.key) {
      console.log("okk")
        Activity.findOne({where: {name: a.name}}).then(response => {
            if(response) {
               res.status(409).send({
                   message: "Activity already exists"
               })
            } else {
              Activity.create(a).then(()=> {
                res.status(201).send({
                  message: 'Created successfully'
                });
            })
        }
      })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        });
    }

})

app.post('/register-students', (req, res) => {
    const student = req.body;
    if(student.name && student.email && student.password && student.gradeYear) {
        Student.findOne({where: {email: student.email}}).then(response => {
            if(response) {
               res.status(409).send({
                   message: "Student email already in use"
               })
            } else {
              Student.create(student).then(()=> {
                res.status(201).send({
                  message: 'Student inserted!'
                });
            })
        }
      })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        });
    }

})



app.post('/create-activity', (req, res) => {
    const activity =req.body;
  if( activity.name && activity.startingDate && activity.endingDate && activity.teacherID && activity.descripton && activity.key){

        Activity.findOne({where: {name: activity.name}}).then(response => {
            if(response) {
               res.status(409).send({
                   message: "Activity already created! Please join it!"
               })
            } else {
              Activity.create(activity).then(()=> {
                res.status(201).send({
                  message: 'Activity was created successfully!'
                });
            })
            //inserare si in ActivitiesList

        }
      })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        });
    }

})
app.post('/join-teacher', (req, res) => {
    const activityList =req.body;
  if( activityList.activityID && activitiesList.teacherID){

            //inserare si in ActivitiesList
            ActivitiesList.create(activityList).then(()=> {
              res.status(201).send({
                message: 'Succesfully joined activity!'
              });
          })

    } else {
        res.status(400).send({
            message: 'Invalid payload'
        });
    }

})

app.post('/login-students',cors(), (req, res) => {
    const conn = req.body;
    Student.findOne({where: {email: conn.email, password: conn.password}}).then(response=> {
        if(response) {
            res.status(200).send(response);
        } else {
            res.status(404).send({
                message: 'Invalid user or password'
            })
        }
    })
})
app.post('/login-teachers', cors(),(request, response) => {
    const credentials = request.body;
    console.log(credentials)
    Teacher.findOne({where: {email: credentials.email, password: credentials.password}}).then(result => {
        if(result) {
            response.status(200).send(result);
        } else {
            response.status(404).send({
                message: 'Invalid credentials'
            })
        }
    })
})
app.post('/get-by-id-activ', cors(),(request, response) => {
    const credentials = request.body;
    console.log(credentials)
    Activity.findOne({where: {id:credentials.id}}).then(result => {
        if(result) {
            response.status(200).send(result);
        } else {
            response.status(404).send({
                message: 'Invalid credentials'
            })
        }
    })
})



app.put('/teachers/update',cors(), (req, res) => {
    const modify = req.body;
    if(modify.email && modify.newemail) {
        Teacher.update({email: modify.newemail}, {where: {email: modify.email}}).then(response => {
            if(response[0]) {
                console.log(response[0]);
            res.status(200).send({
                    message: `Updated: ${response[0]}`
                })
            } else {
                res.status(200).send({
                    message: "No matching records"
                })
            }
        }).catch(err => {
            res.status(500).send(err);
        })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        })
    }
})


app.put('/students/update',cors(), (req, res) => {
    const modify = req.body;
    if( modify.email && modify.newemail) {
        Student.update({email: modify.newemail}, {where: {email: modify.email}}).then(response => {
            if(response[0]) {
                console.log(response[0]);
            res.status(200).send({
                    message: `Updated: ${response[0]}`
                })
            } else {
                res.status(200).send({
                    message: "No matching records"
                })
            }
        }).catch(err => {
            res.status(500).send(err);
        })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        })
    }
})


app.put('/activity/update',cors(), (req, res) => {
    const modify = req.body;
    if( modify.duration && modify.newduration) {
        Activity.update({duration: modify.newduration}, {where: {duration: modify.duration}}).then(response=> {
            if(response[0]) {
                console.log(response[0]);
            res.status(200).send({
                    message: `Updated: ${response[0]}`
                })
            } else {
                res.status(200).send({
                    message: "No matching records"
                })
            }
        }).catch(err => {
            res.status(500).send(err);
        })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        })
    }
})


app.delete('/teachers/delete',cors(), (req, res) => {
    const about = req.body;
    if( about.email) {
        Teacher.destroy({where: about}).then(result => {
            if(result) {
                res.status(200).send({
                    message: `Deleted ${result} rows`
                });
            } else {
                res.status(200).send({
                    message: `No records found matching the criteria`
                })
            }
        }).catch(err => {
            res.status(500).send(err);
        })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        })
    }
})

app.get('/get-activities',cors(), (req, res) => {
    Activity.findAll().then(activities => res.json(activities))
})
app.get('/get-teachers',cors(), (req, res) => {
    Teacher.findAll().then(teachers => res.json(teachers))

})
app.get('/get-students',cors(), (req, res) => {
  Student.findAll().then(teachers => res.json(teachers))

})
//get pentru trimitere id cand primeste email
app.get('/get-teacher-id', (req, res) => {
    //Teacher.findAll().then(teachers => res.json(teachers))
    const core=req.body;
    if(core.email&& core.password)
    {
    Teacher.findOne({where: {email: core.email, password: core.password}}).then(result => {
        if(result) {
            res.json(result.id)
        } else {
            res.status(404).send({
                message: 'Cannot find teacher'
            })
        }
    })
  }else {
    res.status(400).send({
        message: 'Invalid payload'
    })
  }
})

app.post('/get-activity-by-key', (req, res) => {
    //Teacher.findAll().then(teachers => res.json(teachers))
    const a=req.body;
    if(a.key)
    {
    Activity.findOne({where: {key:a.key}}).then(result => {
        if(result) {
            res.json(result)


        } else {
            res.status(404).send({
                message: 'Invalid key'
            })
        }
    })
  }else {
    res.status(400).send({
        message: 'Invalid payload'
    })
  }

})

app.post('/get-activity-by-name', (req, res) => {
    //Teacher.findAll().then(teachers => res.json(teachers))
    const a=req.body;
    if(a.name)
    {
    Activity.findOne({where: {name:a.name}}).then(result => {
        if(result) {
            res.json(result)


        } else {
            res.status(404).send({
                message: 'Invalid name'
            })
        }
    })
  }else {
    res.status(400).send({
        message: 'Invalid payload'
    })
  }

})

app.put('/smile',cors(), (req, res) => {
    const modify = req.body;
    if( modify.name && modify.newsmile) {
        Activity.update({smile: modify.newsmile}, {where: {name: modify.name}}).then(response=> {
            if(response) {
                console.log();
            res.status(200).send({
                    message: `Updated:`
                })
            } else {
                res.status(201).send({
                    message: "No matching records"
                })
            }
        }).catch(err => {
            res.status(500).send(err);
        })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        })
    }
})

app.put('/frown',cors(), (req, res) => {
    const modify = req.body;
    if( modify.name && modify.newfrown) {
        Activity.update({frown: modify.newfrown}, {where: {name: modify.name}}).then(response=> {
            if(response) {
                console.log();
            res.status(200).send({
                    message: `Updated:`
                })
            } else {
                res.status(201).send({
                    message: "No matching records"
                })
            }
        }).catch(err => {
            res.status(500).send(err);
        })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        })
    }
})
app.put('/confuse',cors(), (req, res) => {
    const modify = req.body;
    if( modify.name && modify.newconfuse) {
        Activity.update({confuse: modify.newconfuse}, {where: {name: modify.name}}).then(response=> {
            if(response) {
                console.log();
            res.status(200).send({
                    message: `Updated:`
                })
            } else {
                res.status(201).send({
                    message: "No matching records"
                })
            }
        }).catch(err => {
            res.status(500).send(err);
        })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        })
    }
})
app.put('/suprise',cors(), (req, res) => {
    const modify = req.body;
    if( modify.name && modify.newsuprise) {
        Activity.update({suprise: modify.newsuprise}, {where: {name: modify.name}}).then(response=> {
            if(response) {
                console.log();
            res.status(200).send({
                    message: `Updated:`
                })
            } else {
                res.status(201).send({
                    message: "No matching records"
                })
            }
        }).catch(err => {
            res.status(500).send(err);
        })
    } else {
        res.status(400).send({
            message: 'Invalid payload'
        })
    }
})



app.listen(port, ()=>
{
console.log(`Server is listening on port ${port}`);
}
);
