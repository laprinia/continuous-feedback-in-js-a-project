import React,{useState} from 'react';
import { BrowserRouter as Router,Switch,Link, Route } from 'react-router-dom';
import {AuthContext} from './components/auth'
import Login from './components/Login';
import Signup from './components/Signup';
import './app-style.css';
import Home from './components/Home'
import TeacherDashboard from './components/TeacherDashboard';
import StudentDashboard from './components/StudentDashboard';
import Feedback from './components/Feedback';
import GiveFeedback from './components/GiveFeedback';

function App(props) {
  const [authTokens, setAuthTokens] = useState();

  const setTokens = (data) => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  }
  return (
     <AuthContext.Provider value={{authTokens, setAuthTokens: setTokens }}>
    <Router>
      <div>
        <ul>
          <li>
          <nav className="navbar navbar-dark bg-primary fixed-top">
            <Link className="navbar-brand" to="/">
              Home
            </Link>
            <Link className="navbar-brand" to="/login">
              Login
            </Link>

          </nav>
          </li>
        </ul>
        <div id="topmarg"></div>
        <Route path="/" component={Home} />
        <Route path="/teacher-dashboard" component={TeacherDashboard} />

        <Route
          path='/student-dashboard'
          render={(props) => <StudentDashboard {...props} isAuthed={true} />}
             />
         <Route path="/signup" component={Signup} />
          <Route path="/feedback-received" component={Feedback} />
          <Route path="/give-feedback" component={GiveFeedback} />
         <Route exact path="/login" component={Login} />


      </div>
    </Router>

     </AuthContext.Provider>

  );
}

export default App;
