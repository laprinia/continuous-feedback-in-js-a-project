import React from 'react';


import Listing from './Listing.js';

import {Success,Error,Button2,Title,Card,Form, Input, Button } from "./ActivityForm";
import './style.css';
import amazing from './png/amazings.png'
import crying from './png/cryings.png'
import muted from './png/muteds.png'
import shocked from './png/shockeds.png'
import { Link, Redirect } from "react-router-dom";
import axios from 'axios';
const SERVER = 'http://localhost:3236';
var data
var keyPass
var activity

class StudentDashboard  extends React.Component{

constructor()
{
  super()
  var user=window.localStorage.getItem('user');
  data=JSON.parse(user)
  this.state={
    isError:false,
    isSuccess:false
  }

}
keyChange(e){
keyPass=e.target.value
}
enterKey(e){
  console.log(keyPass)
  axios.post(`${SERVER}/get-activity-by-key`, {

      "key":keyPass
    },
  {headers: {'Content-Type': 'application/json'}}).then(result => {
       if (result.status === 200) {
        console.log(result.data)
        activity=result.data
        var utc = new Date().toJSON().slice(0,10).replace(/-/g,'.');
        var curr=utc.toString();
            if(activity["endingDate"]<curr && activity["startingDate"]>curr)
            {
              this.setState({isError:true})
              this.setState({isSuccess:false})
              window.localStorage.setItem(activity.name,'ok')


            }else {
              this.setState({isSuccess:true})
                this.setState({isError:false})

            }



       } else {

       }
     }).catch(e => {

     });



}

render(){
  return (
<div>
<Card>
<Title>
Hello {data.name}! Please request an activity key and enter it bellow
</Title>
  <Form>

  <Input
    placeholder="Unique Key"

    onChange={this.keyChange.bind(this)}

  />


    <Button2 onClick={this.enterKey.bind(this)}>Join!</Button2>
    <Link to="/give-feedback">->Now give the activity some feedback</Link>
      { this.state.isError &&<Error>The activity is either nonexistent or expired!</Error> }
        { this.state.isSuccess &&<Success>Congrats you joined {activity.name}. It ends on {activity.endingDate}</Success> }

  </Form>

  </Card>
</div>


  );
}
}

export default StudentDashboard;
