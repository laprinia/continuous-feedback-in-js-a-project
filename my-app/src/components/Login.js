import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from 'axios'
import logoImg from "../groups.png";
import { StyledCheckbox,Card, Logo, Form, Input, Button, Error } from "./AuthForm";
import { useAuth } from "./auth";
const SERVER = 'http://localhost:3236';
var isTeacher=false;
var jwt = require('jwt-simple');
var payload = {};




function Login() {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const { setAuthTokens } = useAuth();

  function postLogin() {
  if(isTeacher){
    axios.post(`${SERVER}/login-teachers`, {
         "email":userName,
         "password": password
       }).then(result => {
         if (result.status === 200) {
           window.localStorage.setItem('user',JSON.stringify(result.data))
           payload=result.data
           setLoggedIn(true);
         } else {
           setIsError(true);
         }
       }).catch(e => {
         setIsError(true);
       });
}else{

  axios.post(`${SERVER}/login-students`, {
       "email":userName,
       "password": password
     }).then(result => {
       if (result.status === 200) {
         setAuthTokens(result.data);
         window.localStorage.setItem('user',JSON.stringify(result.data))


         setLoggedIn(true);
       } else {
         setIsError(true);
       }
     }).catch(e => {
       setIsError(true);
     });

}
  }

  if (isLoggedIn &&isTeacher) {

  return <Redirect to="/teacher-dashboard" />;
}else if(isLoggedIn){

return <Redirect to="/student-dashboard" />;

}
  function handleCheck(){
    var checkBox = document.getElementById("mybox");

    if (checkBox.checked == true){
     isTeacher="checked"
}  else{
  isTeacher="unchecked"
}

 }
  return (
    <Card>
      <Logo src={logoImg} />
      <Form>
        <Input
          type="username"
          value={userName}
          onChange={e => {
            setUserName(e.target.value);
          }}
          placeholder="email"
        />
        <Input
          type="password"
          value={password}
          onChange={e => {
            setPassword(e.target.value);
          }}
          placeholder="password"
        />
        <Button onClick={postLogin}>Sign In</Button>
      </Form>
      <label>
         <input type="checkbox"  id="mybox" onChange={handleCheck} />
       <span>Login as a teacher</span>
       </label>
      <Link to="/signup">Don't have an account?</Link>
        { isError &&<Error>The username or password provided were incorrect!</Error> }
    </Card>
  );
}

export default Login;
