import styled from 'styled-components';

const EmojiBox = styled.div`
  padding: 0.5em;
  margin: 0.5em;
  background: ${props => props.inputColor || "papayawhip"};
 font-size: 2rem;
  width: 100%;
  color: #383b40;
  height:30%;
`;



const ActivityBox = styled.div`
  box-sizing: border-box;
  width: 60%;
  height:80px;
  border: 2px solid salmon;
  color:salmon;
  margin: 0 auto;
  padding: 0 2rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const OtherBox = styled.div`
  box-sizing: border-box;
  width: 30%;
  height:50%;
  border: 2px solid salmon;
  color:salmon;
  margin: 0 auto;
  margin-top: 2rem;
  align-items:center;
  padding: 0 2rem;
  display: flex;
  flex-direction: column;

`;

const Form = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Input = styled.input`
  padding: 1rem;
  border: 1px solid #999;
  margin-bottom: 1rem;
  font-size: 0.8rem;
`;

const Button = styled.button`
  background: linear-gradient(to bottom,#5c0556, #3d0239);
  border-color: #3f4eae;
  border-radius: 3px;
  padding: 1rem;
  color: white;
  font-weight: 700;
  width: 100%;
  margin-bottom: 1rem;
  font-size: 0.8rem;
`;
const Button2 = styled.button`
  background: linear-gradient(to bottom,#ff94bf, #f25796);
  border-color:  #f25796;
  border-radius: 3px;
  padding: 1rem;
  color: black;
  font-weight: 700;
  width: 100%;
  margin-bottom: 1rem;
  font-size: 0.8rem;
`;


const Card = styled.div`
  box-sizing: border-box;
  max-width: 410px;
  margin: 0 auto;
  padding: 0 2rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const Title = styled.h1`
  font-size: 1em;
  text-align: center;
  color: green;
`;
const Error = styled.div`
  background-color: white;
  border: 2px solid red;
  color:red;
`;
const Success = styled.div`
  background-color: white;
  border: 2px solid green;
  font-size:1em;

  color: green;
`;

export {OtherBox,EmojiBox,ActivityBox,Success,Button2,Error,Title,Card,Form, Input, Button };
