import React from 'react';
import Listing from './Listing.js';
import Add2 from './Add2.js';
import {Success,Error,Title,Card,Form, Input, Button } from "./ActivityForm";
import { Link, Redirect } from "react-router-dom";

import axios from 'axios'
const SERVER = 'http://localhost:3236';
var data
var name,starting,ending,descripton,key
var isError=false

class TeacherDashboard extends React.Component {

 constructor(){
super()
var user=window.localStorage.getItem('user');
data=JSON.parse(user)
this.state={
  isError:false,
  isSuccess:false
}


 }
createActiv(){
   console.log(name)

  axios.post(`${SERVER}/make-activity`, {
      "name": name,
      "startingDate": starting,
       "endingDate": ending,
        "teacherID": data.id,
      "description":descripton,
      "key":key
    },
  {headers: {'Content-Type': 'application/json'}}).then(result => {
       if (result.status === 201) {

         this.setState({isError:false})
         this.setState({isSuccess:true})
       } else {
         this.setState({isError:true})
         this.setState({isSuccess:false})
       }
     }).catch(e => {
       this.setState({isError:true})
       this.setState({isSuccess:false})

     });




}
nameChange(e){

name=e.target.value

}
startingChange(e){
starting=e.target.value

}
endingChange(e){
ending=e.target.value
}
descriptonChange(e){
descripton=e.target.value
}
keyChange(e){
key=e.target.value
}


    render() {
      return (
          <div>

            <Card>
            <Title>
     Hello {data.fullName}! Please enter details about your activity bellow
   </Title>
              <Form>

              <Input
                placeholder="Name for activity"

                onChange={this.nameChange.bind(this)}

              />
                <Input
                  placeholder="Starting date (YYYY.MM.DD)"

                  onChange={this.startingChange.bind(this)}

                />
                <Input
                  placeholder="Ending date (YYYY.MM.DD)"

                  onChange={this.endingChange.bind(this)}

                />
                <Input
                  placeholder="A short description"

                  onChange={this.descriptonChange.bind(this)}

                />
                <Input
                  placeholder="Activity key"

                  onChange={this.keyChange.bind(this)}

                />

                <Button onClick={this.createActiv.bind(this)}>Create Activity</Button>

                <Link to="/feedback-received">->View continuous feedback given by students</Link>

                { this.state.isSuccess &&<Success>Successfully created {name}, that will expire on {ending} with the unique key: {key}</Success> }
                { this.state.isError &&<Error>Error:Either activity exists or data is not filled</Error> }
              </Form>

              </Card>
          </div>
      )
    }
}

export default TeacherDashboard;
