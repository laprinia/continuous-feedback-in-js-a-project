import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from 'axios'
import logoImg from "../groups.png";
import { StyledCheckbox,Card, Logo, Form, Input, Button, Error } from "./AuthForm";
import { useAuth } from "./auth";
const SERVER = 'http://localhost:3236';
var isTeacher=false;
var jwt = require('jwt-simple');
var payload = {};




function Signup() {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);
  const [userName, setUserName] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");

  const { setAuthTokens } = useAuth();

  function postSignup() {
  if(isTeacher){
    axios.post(`${SERVER}/register-teachers`, {
         "email":userName,
         "fullName":name,
         "password": password
       }).then(result => {
         if (result.status === 201) {
           window.localStorage.setItem('user',JSON.stringify(result.data))
           payload=result.data
           setLoggedIn(true);
         } else {
           setIsError(true);
         }
       }).catch(e => {
         setIsError(true);
       });
}else{

  axios.post(`${SERVER}/register-students`, {
       "email":userName,
       "name": name,
       "password": password,
       "gradeYear": "0"
     }).then(result => {
       if (result.status === 201) {
         console.log(result.data)
         setAuthTokens(result.data);
         window.localStorage.setItem('user',JSON.stringify(result.data))


         setLoggedIn(true);
       } else {
         setIsError(true);
       }
     }).catch(e => {
       setIsError(true);
     });

}
  }

  if (isLoggedIn &&isTeacher) {

  return <Redirect to="/login" />;
}else if(isLoggedIn){

return <Redirect to="/login" />;

}
  function handleCheck(){
    var checkBox = document.getElementById("mybox");

    if (checkBox.checked == true){
     isTeacher="checked"
}  else{
  isTeacher="unchecked"
}

 }
  return (
    <Card>
      <Logo src={logoImg} />
      <Form>
        <Input
          type="username"

          onChange={e => {
            setUserName(e.target.value);
          }}
          placeholder="email"
        />
        <Input
          type="text"

          onChange={e => {
            setName(e.target.value);
          }}
          placeholder="full name"
        />
        <Input
          type="password"

          onChange={e => {
            setPassword(e.target.value);
          }}
          placeholder="password"
        />
        <Button onClick={postSignup}>Register</Button>
      </Form>
      <label>
         <input type="checkbox"  id="mybox" onChange={handleCheck} />
       <span>Register as a teacher</span>
       </label>
      <Link to="/login">Already have an account?</Link>
        { isError &&<Error>The credentials are not valid</Error> }
    </Card>
  );
}

export default Signup;
