import React from 'react';

export default class Add extends React.Component{
    render(){
        return(
            <div>
                <form>
                    <div class="form-group">
                        <label for="Name">Below enter the ID of an activity you want to enroll in</label>
                        <input type="text" class="form-control" id="Name" aria-describedby="nameHelp" placeholder="Activity ID" />
                    </div>
                    <button type="submit" class="btn btn-primary">Join an Activity</button>
                 </form>
            </div>
        );
    }
}
