import React from 'react';

export default class Add2 extends React.Component{
    render(){
        return(
            <div>
                <form>
                    <div class="form-group">
                        <label for="Name">Start a new activity or join an already existing one</label>
                        <input type="text" class="form-control" id="Name" aria-describedby="nameHelp" placeholder="Activity ID" />
                    </div>
                    <button type="submit" class="btn btn-primary">Start an Activity</button>
                    <button type="submit" class="btn btn-primary">Join an Activity</button>
                 </form>
            </div>
        );
    }
}
