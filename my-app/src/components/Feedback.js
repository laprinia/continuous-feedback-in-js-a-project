import React from 'react';


import {OtherBox,EmojiBox,ActivityBox,Title,Card,Form, Input, Button } from "./ActivityForm";
import { Link, Redirect } from "react-router-dom";
import axios from 'axios';
const SERVER = 'http://localhost:3236';
var name;
var activity;

class Feedback extends React.Component {
  constructor(){
    super()
    this.state={
      isOK: false
    }
  }
nameChange(e){
  name=e.target.value
}
 viewFeedBack(e){
 console.log(name)
 axios.post(`${SERVER}/get-activity-by-name`, {

     "name":name
   },
 {headers: {'Content-Type': 'application/json'}}).then(result => {
      if (result.status === 200) {
       console.log(result.data)
       activity=result.data
       this.setState({isOK:true})


      } else {

      }
    }).catch(e => {

    });

}

    render() {
      return (
          <div>

            <Card>
            <Title>
            Select the name of the activity you want to view
           </Title>
              <Form>

              <Input placeholder="Activity Name" onChange={this.nameChange.bind(this)} />


                <Button onClick={this.viewFeedBack.bind(this)}>Show Feedback</Button>
                 <Link to="/teacher-dashboard">->Go back to creating</Link>

              </Form>
              </Card>
               { this.state.isOK &&<ActivityBox>{activity.name} was found! Starts on {activity.startingDate}, ends on {activity.endingDate} Description: {activity.description}</ActivityBox> }

               <div> </div>
               { this.state.isOK &&<OtherBox>
                 {this.state.isOK&&<EmojiBox inputColor="#fff25e">😄:{activity.smile }</EmojiBox>}
                 {this.state.isOK&&<EmojiBox inputColor="#8ac7ed">😓:{activity.frown} </EmojiBox>}
                 {this.state.isOK&&<EmojiBox inputColor="#ffb0ad">😮:{activity.suprise}</EmojiBox>}
                 {this.state.isOK&&<EmojiBox inputColor="#81eb71">😕:{activity.confuse} </EmojiBox>}


                 </OtherBox> }



          </div>
      )
    }
}

export default Feedback;
