import React from 'react';


import Listing from './Listing.js';

import {Success,Error,Title,Card,Form, Input, Button } from "./ActivityForm";
import './style.css';
import amazing from './png/amazings.png'
import crying from './png/cryings.png'
import muted from './png/muteds.png'
import shocked from './png/shockeds.png'
import { Link, Redirect } from "react-router-dom";
import axios from 'axios';
const SERVER = 'http://localhost:3236';

var emotion
var activity
var name






class GiveFeedback  extends React.Component{
  constructor(){
   super()
   this.state={
     isError:false,
     isSuccess:false
   }


  }
changeName(e){
  name=e.target.value
}
onClick(e){
  console.log(name)
  var token=window.localStorage.getItem(name)
  if(token='ok')
  {
  axios.post(`${SERVER}/get-activity-by-name`, {

      "name":name
    },
  {headers: {'Content-Type': 'application/json'}}).then(result => {
       if (result.status === 200) {
        console.log(result.data)
        activity=result.data


        var utc = new Date().toJSON().slice(0,10).replace(/-/g,'.');
        var curr=utc.toString();
            if(activity["endingDate"]<curr)
            {
              this.setState({isError:true})
              this.setState({isSuccess:false})


            }else {
              this.setState({isSuccess:true})
                this.setState({isError:false})

            }


       } else {
         this.setState({isSuccess:false})
         this.setState({isError:true})
       }
     }).catch(e => {
       this.setState({isSuccess:false})
       this.setState({isError:true})
     });
   }else{
     this.setState({isSuccess:false})
     this.setState({isError:true})
   }

}
amazing(e)
{

if(this.state.isSuccess){
  axios.put(`${SERVER}/smile`, {
    name: activity.name,
    newsmile: activity.smile++

    },
  {headers: {'Content-Type': 'application/json'}}).then(result => {
       if (result.status === 200) {
        console.log(result.data)

            }

        else {

       }
     }).catch(e => {

     });

}
}
crying(e)
{

if(this.state.isSuccess){
  axios.put(`${SERVER}/frown`, {
    name: activity.name,
    newfrown: ++activity.frown

    },
  {headers: {'Content-Type': 'application/json'}}).then(result => {
       if (result.status === 200) {
        console.log(result.data)

            }

        else {

       }
     }).catch(e => {

     });
}
}
muted(e){

if(this.state.isSuccess){
  axios.put(`${SERVER}/confuse`, {
    name: activity.name,
    newconfuse: ++activity.confuse

    },
  {headers: {'Content-Type': 'application/json'}}).then(result => {
       if (result.status === 200) {
        console.log(result.data)

            }

        else {

       }
     }).catch(e => {

     });

}
}
shocked(e)
{

if(this.state.isSuccess){
  axios.put(`${SERVER}/suprise`, {
    name: activity.name,
    newsuprise: ++activity.suprise

    },
  {headers: {'Content-Type': 'application/json'}}).then(result => {
       if (result.status === 200) {
        console.log(result.data)

            }

       else {

       }
     }).catch(e => {

     });
}
}


render(){
  return (

  <container>


              <Card>
              <Title>
              Select the name of the activity you want to review</Title>
                <Form>

                <Input  placeholder="Activity Name"  onChange={this.changeName.bind(this)}/>


                  <Button onClick={this.onClick.bind(this)}>Find Activity</Button>

                </Form>
                <Link to="/student-dashboard">->Go back to joining</Link>
                { this.state.isError &&<Error>The activity is nonexistent or you are not enrolled in it!</Error> }
                  { this.state.isSuccess &&<Success>Time to review {activity.name}! Pick your thoughts below:</Success> }
                </Card>
                <div id="top"></div>
                <div id="bottom">
                  <div class="inner">
                  <img onClick={this.amazing.bind(this)}  src={amazing} className="App-logo" alt="logo" />

                  </div>
                  <div class="inner">
                  <img onClick={this.crying.bind(this)}  src={crying} className="App-logo" alt="logo" />

                  </div>
                  <div class="inner">
                  <img onClick={this.muted.bind(this)}  src={muted} className="App-logo" alt="logo" />

                  </div>
                  <div class="inner">
                  <img onClick={this.shocked.bind(this)}  src={shocked} className="App-logo" alt="logo" />

                  </div>
                </div>


 </container>

  );
}
}

export default GiveFeedback;
